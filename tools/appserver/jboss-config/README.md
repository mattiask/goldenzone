JBoss Configuration
===================
This module defines different JBoss configurations used in other modules which installs a JBoss AS.

The configurations are available as zip files and can be included in JBoss AS installations as follows:

    <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
            <execution>
                <id>unpack</id>
                <phase>process-test-resources</phase>
                <goals>
                    <goal>unpack</goal>
                </goals>
                <configuration>
                    <artifactItems>
                        <artifactItem>
                            <groupId>org.jboss.as</groupId>
                            <artifactId>jboss-as-dist</artifactId>
                            <version>${jboss.version}</version>
                            <type>zip</type>
                            <overWrite>false</overWrite>
                            <outputDirectory>${server.location}</outputDirectory>
                        </artifactItem>
                        <artifactItem>
                            <groupId>${project.parent.groupId}</groupId>
                            <artifactId>jboss-config</artifactId>
                            <version>${project.parent.version}</version>
                            <classifier>${system.type}</classifier> <!-- test or prod -->
                            <type>zip</type>
                            <overWrite>false</overWrite>
                            <outputDirectory>${server.location}/jboss-as-${jboss.version}</outputDirectory>
                        </artifactItem>
                    </artifactItems>
                </configuration>
            </execution>
        </executions>
    </plugin>