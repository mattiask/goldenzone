/*
 * Created by Daniel Marell 13-04-18 2:36 PM
 */
package com.caglabs.goldenzone.helloservice.impl;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Enables @Injection of EntityManager
 * CDI krånglar i Glassfish, testar med denna /Daniel
 */
public class EntityManagerProducer {
    @Produces
    @PersistenceContext(unitName = "GoldenzonePU")
    private EntityManager em;
}
