package com.caglabs.goldenzone.helloservice.client;

public interface Hello {
    public static class HelloException extends Exception {
        public HelloException(String message) {
            super(message);
        }

        public HelloException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    String sayHello(String name) throws HelloException;
}